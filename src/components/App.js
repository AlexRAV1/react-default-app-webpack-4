import React from 'react';
import {hot} from 'react-hot-loader';

class App extends React.Component {
    constructor() {
        super();

        this.state = {
            hello: 'Hello world!'
        }
    }
    render() {
        return <div>{this.state.hello}</div>
    }
}

export default hot(module)(App);